import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicModule } from '@ionic/angular';
import {
  dataStoreFactory,
  DATA_STORE_TOKEN,
} from '../auth/data-store/data-store.factory';
import { AccountService } from '../ledger/entities/account/account.service';
import { MaterialModule } from '../material/material.module';

import { TransactionTableComponent } from './transaction-table.component';

describe('TransactionTableComponent', () => {
  let component: TransactionTableComponent;
  let fixture: ComponentFixture<TransactionTableComponent>;
  let service: AccountService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        IonicModule.forRoot(),
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
      declarations: [TransactionTableComponent],
      providers: [
        { provide: DATA_STORE_TOKEN, useFactory: dataStoreFactory },
        {
          provide: MatDialog,
          useValue: {},
        },
      ],
    }).compileComponents();
    service = TestBed.inject(AccountService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
