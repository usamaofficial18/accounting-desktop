import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { from, Observable } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { DialogData, FilterObject } from '../common/interface';
import { DialogOverviewExampleDialog } from '../common/record-dialog/update-transaction-dialog';
import { AccountService } from '../ledger/entities/account/account.service';

@Component({
  selector: 'app-transaction-table',
  templateUrl: './transaction-table.component.html',
  styleUrls: ['./transaction-table.component.scss'],
})
export class TransactionTableComponent implements OnInit {
  @ViewChild(MatPaginator)
  paginator: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort;

  @Input()
  transaction_name: string;

  displayedColumns: string[] = ['sr_no', 'name', 'cost', 'date', 'reason'];
  dataSource: MatTableDataSource<DialogData>;
  filterDate = new FormControl();
  filterName = new FormControl();
  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });
  nameOptions: Observable<any>;

  constructor(
    private accountService: AccountService,
    public dialog: MatDialog,
  ) {}

  getUpdate(event?) {
    const query = {} as FilterObject;
    query.status = this.transaction_name;
    const regExp = new RegExp(this.filterName.value);
    if (this.filterName.value) query.name = { $regex: regExp };
    if (this.range.controls.start.value && this.range.controls.end.value) {
      query.date = {
        $gte: new Date(this.range.controls.start.value.toDateString()),
        $lte: new Date(this.range.controls.end.value.toDateString()),
      };
    } else {
      if (this.filterDate.value) {
        query.date = new Date(this.filterDate.value.toDateString());
      }
    }
    this.accountService
      .find(query, event.pageSize, event.pageIndex)
      .then((next: any) => {
        this.dataSource = new MatTableDataSource([...next]);
        this.setDataSourceLength();
      });
  }
  ngAfterViewInit() {
    this.setDataSourceLength();
  }

  setDataSourceLength() {
    this.accountService
      .count({ status: this.transaction_name })
      .then((count: number) => {
        this.dataSource.data.length = count;
      });
  }

  ngOnInit() {
    this.accountService
      .find({ status: this.transaction_name }, 10, 0)
      .then((next: any) => {
        this.dataSource = new MatTableDataSource([...next]);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.setDataSourceLength();
      });
    this.getTotalCost();

    this.nameOptions = this.filterName.valueChanges.pipe(
      startWith(''),
      switchMap(value => {
        const reg = new RegExp(value);
        return from(
          this.accountService.find({ status: 'Name', name: { $regex: reg } }),
        );
      }),
    );
  }

  updateDataSource(transaction_name: string) {
    this.accountService
      .find(
        { status: transaction_name },
        this.paginator.pageSize,
        this.paginator.pageIndex,
      )
      .then((next: any) => {
        this.dataSource = new MatTableDataSource([...next]);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.setDataSourceLength();
      });
  }
  /** Gets the total Transaction of all transactions. */
  getTotalCost() {
    const total: DialogData[] = this.dataSource?.data
      ? this.dataSource?.data
      : [];
    return total.map(t => t.cost).reduce((acc, value) => acc + value, 0);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  removeTransaction(remove) {
    this.accountService.remove({ _id: remove._id }).then(next => {
      this.ngOnInit();
    });
  }

  openDialog(transaction): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '50vh',
      data: {
        name: transaction.name,
        cost: transaction.cost,
        date: transaction.date,
        status: transaction.status,
        reason: transaction.reason,
        _id: transaction._id,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }
}
