import { Component, Inject } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogData, STATUS } from '../interface';
import { AccountService } from '../../ledger/entities/account/account.service';
import { startWith, switchMap } from 'rxjs/operators';
import { from, Observable } from 'rxjs';

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'update-transaction-dialog.html',
})
export class DialogOverviewExampleDialog {
  transactionStatus: string[] = [STATUS.CREDIT, STATUS.DEBIT];
  transactionHistoryForm: FormGroup;
  nameOptions: Observable<any>;
  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private accountService: AccountService,
    private snackbar: MatSnackBar,
    private formBuilder: FormBuilder,
  ) {
    this.transactionHistoryForm = this.formBuilder.group({
      status: new FormControl('', []),
      name: new FormControl('', []),
      date: new FormControl('', []),
      cost: new FormControl('', []),
      reason: new FormControl('', []),
    });
    this.valueAndValidity(data);
  }

  valueAndValidity(data?) {
    if (this.data._id) {
      Object.keys(this.transactionHistoryForm.controls).forEach(element => {
        this.transactionHistoryForm.get(element).setValue(data[element]);
      });
    } else {
      Object.keys(this.transactionHistoryForm.controls).forEach(element => {
        this.transactionHistoryForm
          .get(element)
          .setValidators(Validators.required);
        this.transactionHistoryForm.get(element).updateValueAndValidity();
      });
    }
  }

  ngOnInit() {
    this.nameOptions = this.transactionHistoryForm.controls.name.valueChanges.pipe(
      startWith(''),
      switchMap(value => {
        const reg = new RegExp(value);
        return from(
          this.accountService.find({ status: 'Name', name: { $regex: reg } }),
        );
      }),
    );
  }

  updateEntry(type) {
    switch (type) {
      case 'update':
        this.updateTransaction();
        break;

      case 'delete':
        this.deleteTransaction();
        break;
      default:
        this.snackbar.open('Please select the correct option', 'Close', {
          duration: 3000,
        });
        break;
    }
  }

  updateTransaction() {
    const data = {} as DialogData;
    if (this.transactionHistoryForm.controls.cost.value !== this.data.cost)
      data.cost = this.transactionHistoryForm.controls.cost.value;
    if (this.transactionHistoryForm.controls.name.value !== this.data.name)
      data.name = this.transactionHistoryForm.controls.name.value;
    if (
      new Date(
        this.transactionHistoryForm.controls.date.value.toDateString(),
      ) !== this.data.date
    )
      data.date = new Date(
        this.transactionHistoryForm.controls.date.value.toDateString(),
      );
    if (this.transactionHistoryForm.controls.status.value !== this.data.status)
      data.status = this.transactionHistoryForm.controls.status.value;
    if (this.transactionHistoryForm.controls.reason.value !== this.data.reason)
      data.reason = this.transactionHistoryForm.controls.reason.value;

    if (this.data._id) {
      data._id = this.data._id;
      this.accountService.update(data, data._id).then(next => {
        this.snackbar.open('Record Updated Successfully', 'Close', {
          duration: 3000,
        });
        this.dialogRef.close();
      });
    } else {
      this.accountService.save(data).then(next => {
        this.snackbar.open('Record Added Successfully', 'Close', {
          duration: 3000,
        });
        Object.keys(this.transactionHistoryForm.controls).forEach(element => {
          if (element !== 'date') {
            this.transactionHistoryForm.controls[element].setValue(null);
            this.transactionHistoryForm.controls[element].markAsUntouched();
          }
        });
      });
    }
  }

  deleteTransaction() {
    const data = {} as DialogData;
    data._id = this.data._id;
    this.accountService.remove({ _id: data._id }).then(next => {
      this.snackbar.open('Record Removed Successfully', 'Close', {
        duration: 3000,
      });
      this.dialogRef.close(data);
    });
  }
}

@Component({
  selector: 'delete-dialog',
  templateUrl: 'delete-dialog.html',
})
export class DeleteDialog {
  constructor(
    public dialogRef: MatDialogRef<DeleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private accountService: AccountService,
  ) {}

  ngOnInit() {}

  deleteDatabase() {
    this.accountService.remove({}).then(next => {
      this.dialogRef.close();
    });
  }

  deleteEntry() {
    this.accountService.remove({ status: { $ne: 'Name' } }).then(next => {
      this.dialogRef.close();
    });
  }

  deleteName() {
    this.accountService.remove({ status: 'Name' }).then(next => {
      this.dialogRef.close();
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
