export interface DialogData {
  cost: number;
  name: string;
  date: Date;
  status: string;
  reason: string;
  _id?: any;
}
export enum STATUS {
  DEBIT = 'Debit',
  CREDIT = 'Credit',
}

export class FilterObject {
  name: any;
  status: string;
  date: any;
  fromDate: Date;
  toDate: Date;
}

export class AccountSummary {
  closing: number;
  creditTotal: number;
  debitTotal: number;
  difference: number;
}
