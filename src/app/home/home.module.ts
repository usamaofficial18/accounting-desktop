import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { AddNameDialog, HomePage } from './home.page';
import { MaterialModule } from '../material/material.module';
import { RecordDialogModule } from '../common/record-dialog/record-dialog.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaterialModule,
    ReactiveFormsModule,
    RecordDialogModule,
    FlexLayoutModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage,
      },
    ]),
  ],
  declarations: [AddNameDialog, HomePage],
})
export class HomePageModule {}
