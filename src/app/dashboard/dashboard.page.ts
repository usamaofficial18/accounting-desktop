import { Component, OnInit } from '@angular/core';
import { AccountService } from '../ledger/entities/account/account.service';
import { MatTableDataSource } from '@angular/material/table';
import { DialogData, STATUS } from '../common/interface';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialog } from '../common/record-dialog/update-transaction-dialog';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  transactionStatus: string[] = [STATUS.CREDIT, STATUS.DEBIT];
  displayedColumns: string[] = ['name', 'cost'];
  dataSource: MatTableDataSource<DialogData>;
  searchValue: string;
  transaction_name: string;
  closing: number = 0;
  creditTotal: number = 0;
  debitTotal: number = 0;
  difference: number = 0;

  constructor(
    private readonly accountService: AccountService,
    private readonly router: Router,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.accountService.find({}, 10, 0).then((next: any) => {
      this.dataSource = new MatTableDataSource([...next]);
    });
    this.findOpening().then(totalAmount => {
      this.closing = totalAmount.credit - totalAmount.debit;
    });
    this.getFilteredTodayTransactions().then(totalAmountToday => {
      this.creditTotal = totalAmountToday.credit;
      this.debitTotal = totalAmountToday.debit;
      this.difference =
        totalAmountToday.credit + this.closing - totalAmountToday.debit;
    });
  }

  async findOpening() {
    let credit: number = 0;
    let debit: number = 0;
    const today = new Date();
    const yesterday = new Date(today);
    yesterday.setDate(yesterday.getDate() - 1);
    credit = await this.getFilteredTransactions(STATUS.CREDIT, {
      $lte: new Date(yesterday.toDateString()),
    });
    debit = await this.getFilteredTransactions(STATUS.DEBIT, {
      $lte: new Date(yesterday.toDateString()),
    });
    return { credit, debit };
  }

  getFilteredTransactions(status, date) {
    return this.accountService.find({ status, date }).then((next: any) => {
      return this.getCost(next);
    });
  }

  async getFilteredTodayTransactions() {
    const today = new Date(new Date().toDateString());
    const credit = await this.getFilteredTransactions(STATUS.CREDIT, today);
    const debit = await this.getFilteredTransactions(STATUS.DEBIT, today);
    return { credit, debit };
  }

  reloadPage() {
    window.location.reload();
  }

  deleteDatabase() {
    const dialogRef = this.dialog.open(DeleteDialog, {
      width: 'auto',
      data: {},
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  navigateHome() {
    this.router.navigate(['home']);
  }

  getCost(next: any) {
    let value: number = 0;
    next.forEach(element => {
      value += element.cost;
    });
    return value;
  }
}
