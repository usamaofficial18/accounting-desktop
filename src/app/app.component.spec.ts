import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';

import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';
import {
  dataStoreFactory,
  DATA_STORE_TOKEN,
} from './auth/data-store/data-store.factory';
import { AccountService } from './ledger/entities/account/account.service';

describe('AppComponent', () => {
  let service: AccountService;
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AppComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule],
        providers: [
          { provide: DATA_STORE_TOKEN, useFactory: dataStoreFactory },
          {
            provide: MatSnackBar,
            useValue: {},
          },
        ],
      }).compileComponents();
      service = TestBed.inject(AccountService);
    }),
  );

  it(
    'should create the app',
    waitForAsync(() => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    }),
  );
});
