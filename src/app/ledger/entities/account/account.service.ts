import { Inject, Injectable } from '@angular/core';
import { DATA_STORE_TOKEN } from '../../../auth/data-store/data-store.factory';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  model;
  constructor(
    @Inject(DATA_STORE_TOKEN)
    private readonly db: any,
  ) {
    this.model = new this.db('account', {}, {});
  }

  async save(docs) {
    return new Promise((resolve, reject) => {
      this.model.save(docs, (err, docs) => {
        if (err) {
          return reject(err);
        }
        return resolve(docs);
      });
    });
  }

  async findOne(query) {
    return new Promise((resolve, reject) => {
      this.model.findOne(query, (err, doc) => {
        if (err) {
          return reject(err);
        }
        return resolve(doc);
      });
    });
  }

  async update(query, _id) {
    return new Promise((resolve, reject) => {
      this.model.update({ _id }, { $set: query }, (err, doc) => {
        if (err) {
          return reject(err);
        }
        return resolve(doc);
      });
    });
  }

  async find(query, pageSize?: number, pageIndex?: number) {
    return new Promise((resolve, reject) => {
      this.model
        .find(query)
        .sort({ date: 1 })
        .skip(pageIndex * pageSize)
        .limit(pageSize)
        .exec((err, doc) => {
          if (err) {
            return reject(err);
          }
          return resolve(doc);
        });
    });
  }

  async count(query) {
    return new Promise((resolve, reject) => {
      this.model
        .find(query)
        .count()
        .exec((err, doc) => {
          if (err) {
            return reject(err);
          }
          return resolve(doc);
        });
    });
  }

  async remove(query) {
    return new Promise((resolve, reject) => {
      this.model.remove(query, { multi: true }, (err, doc) => {
        if (err) {
          return reject(err);
        }
        return resolve(doc);
      });
    });
  }
}
