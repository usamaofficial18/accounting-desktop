// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  // Configure Variables
  appURL: 'http://cap.localhost:4200',
  authorizationURL: 'https://accounts.castlecraft.in/oauth2/confirmation',
  authServerURL: 'https://accounts.castlecraft.in',
  callbackProtocol: 'castlecraft',
  callbackUrl: 'http://cap.localhost:4200/callback',
  clientId: '769a3c75-d719-409f-afe6-ac8cc13e65c8',
  profileURL: 'https://accounts.castlecraft.in/oauth2/profile',
  revocationURL:
    'https://myaccount.castlecraft.in/connected_device/revoke_token',
  tokenURL: 'https://accounts.castlecraft.in/oauth2/token',
  isAuthRequiredToRevoke: true,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
