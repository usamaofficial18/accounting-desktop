export const environment = {
  production: true,

  // Configure Variables
  appURL: 'http://cap.localhost:4200',
  authorizationURL: 'https://accounts.castlecraft.in/oauth2/confirmation',
  authServerURL: 'https://accounts.castlecraft.in',
  callbackProtocol: 'castlecraft',
  callbackUrl: 'http://cap.localhost:4200/callback',
  clientId: 'client_id',
  profileURL: 'https://accounts.castlecraft.in/oauth2/profile',
  revocationURL: 'https://accounts.castlecraft.in/oauth2/revoke',
  scope: 'profile openid email roles phone',
  tokenURL: 'https://accounts.castlecraft.in/oauth2/token',
  isAuthRequiredToRevoke: true,
};

// client_id is available for public, client_secret is to be protected.
